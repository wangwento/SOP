package com.gitee.sop.websiteserver.vo;

import lombok.Data;

/**
 * @author tanghc
 */
@Data
public class DocModuleVO {
    private String module;
}
